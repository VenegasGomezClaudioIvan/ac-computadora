```plantuml
@startwbs

* Von-Neumann vs Hardvard
** Von-newmann 
*** Persona
**** Nacio el 28 de diciembre de 1903 en Budapest, Hungria \nmurio el 8 de febrero de 1987 \nes el creador de la arquitectura de computadoras actuales \n propuso el bit como unidad fundamental de memoria
*** Modelo
**** Arquitectura
***** Los datos y las instrucciones(secuencia de control), Se almacenan en una misma memoria de lectura/escritura.
***** No se pueden diferenciar entre datos e instrucciones al examinar una posición de memoria (Location).
***** Los contenidos de la memoria son direccionados por su ubicación(location), sin importar el tipo de datos contenido all.
***** La ejecución ocurre en modo secuencial mediante la lectura de instrucciones consecutivas desde la memoria.
***** se dividen en
****** Procesador 
****** Dispositivos de almacenamiento
****** Dispositivos I/O
****** Buses

**** Limitaciones
***** La limitación de la longitud de las instrucciones por el bus de datos, que hace que el microprocesador tenga que realizar varios accesos a memoria para buscar instrucciones complejas.
***** La limitación de la velocidad de operación a causa del bus único para datos e instrucciones que no deja acceder simultáneamente a unos y otras, lo cual impide superponer ambos tiempos de acceso.

** Hardvard
*** Fue creado en el año 1944
*** Arquitectura
**** Las instrucciones y los datos se almacenan en caches separadas para mejorar el rendimiento.
**** Por otro lado, tiene el inconveniente de tener que dividir la cantidad de cach entre los dos, por lo que funciona mejor sólo cuando la frecuencia de lectura de instrucciones y de datos es aproximadamente la misma.
**** Esta arquitectura suele utilizarse en DSPs, o procesador de señal digital, usados habitualmente en productos para procesamiento de audio y video.

*** Ventajas
**** El tamaño de las instrucciones no está relacionado con el de los datos, y por lo tanto puede ser optimizado para que cualquier instrucción ocupe una sola posición de memoria de programa, logrando así mayor velocidad y menor longitud de programa.
**** El tiempo de acceso a las instrucciones puede superponerse con el de los datos, logrando una mayor velocidad en cada operación.

@endwbs
```

```plantuml
@startwbs

* Super computadoras en Mexico
** IBM 650
*** La super computadora  fue adquirida por la UNAM en 1958
*** Memoria de 2 Kb
*** Su funcion era a base de bulbos 
** CRAY 432
*** La UNAM la adquiere en 1991
*** tuvo un tiempo de vida alrededor de 10 años
** KanBalam
*** Inicia su funcionamiento en 2007
*** Realizaba 7 millones de operaciones por segundo 
** Miztli
*** La UNAM la pone en funcioamiento en 2012
*** Tiene una velocidad de 228 Teraflops
** Atócatl
*** Instalada durante el 2011 en el Instituto de Astronomía de la UNAM
*** Utilizados para el estudio del universo mediante simulaciones
** Abacus
*** Adquirida por el Cinvestav en el 2014
*** se encontraba entre las 150 más rápidas de ese año.
*** el equipo cuenta con 8,904 núcleos
*** 100 GPU K40 de Nvidia
*** 1.2 Petabytes de almacenamiento
*** 40TB de memoria RAM
*** El equipo es capaz de alcanzar los 400 Teraflops.
@endwbs
```
